"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function cargarParte(id, url) {
  var cont = $(id);
  return new Promise(function (resolve, reject) {
    jQuery.ajax({
      url: url
    }).done(function (data) {
      cont.html(data);
      resolve();
    });
  });
}

function fetchFile(url) {
  return new Promise(function (resolve, reject) {
    jQuery.ajax({
      url: url
    }).done(function (data) {
      resolve(data);
    });
  });
}

function loadCSS() {
  return _loadCSS.apply(this, arguments);
}

function _loadCSS() {
  _loadCSS = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var list, i, path, style;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            list = ["/css/styles.css"];

            for (i = 0; i < list.length; i++) {
              path = list[i];
              style = document.createElement("link");
              style.setAttribute("rel", "stylesheet");
              style.setAttribute("href", path);
              document.head.appendChild(style);
            }

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _loadCSS.apply(this, arguments);
}

function cargarCabeceraYPie() {
  return _cargarCabeceraYPie.apply(this, arguments);
}

function _cargarCabeceraYPie() {
  _cargarCabeceraYPie = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return cargarParte("#myNav", "/partes/cabecera.html");

          case 2:
            _context3.next = 4;
            return cargarParte("#myFooter", "/partes/pie.html");

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _cargarCabeceraYPie.apply(this, arguments);
}

_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return cargarCabeceraYPie();

        case 2:
          _context.next = 4;
          return loadCSS();

        case 4:
          localData.configure_nav();

        case 5:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}))();
