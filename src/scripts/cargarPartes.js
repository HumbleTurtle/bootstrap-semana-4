function cargarParte( id, url ) {
    var cont = $(id);

    return new Promise( function( resolve, reject ) {
        jQuery.ajax({
            url
        }).done( function(data) {
            cont.html(data);
            resolve();
        });
    });
}

function fetchFile( url ) {

    return new Promise( function( resolve, reject ) {
        jQuery.ajax({
            url
        }).done( function(data) {
            resolve( data );
        });
    });
    
}

async function loadCSS () {
 
    var list = [
        "/css/styles.css"
    ];


    for( var i =0; i < list.length; i++) {
        var path = list[i];
        
        let style = document.createElement("link");

        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", path );
    
        document.head.appendChild(style);
    }


}

async function cargarCabeceraYPie () {
    await cargarParte("#myNav",   "/partes/cabecera.html");
    await cargarParte("#myFooter", "/partes/pie.html");
}

( async function () {
           
    await cargarCabeceraYPie();
    await loadCSS();

    localData.configure_nav();

}) ();