module.exports = function(grunt) {

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
    
    grunt.initConfig({
        
        sass: {
            dist:  {
                files: [{
                    expand: true,
                    cwd: 'src/scss',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['src/scss/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'src/css/*.css',
                        'src/*.html',
                        'src/scripts/*.js'
                    ]
                }
            },
            options: {
                watchTask: true,

                server: {
                    baseDir: [ './src' ]
                },

                serveStatic: [{
                    route: ['/node_modules'],
                    dir: './node_modules'
                }]

            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './src',
                    src: 'imgs/*.{png,gif,jpg,jpeg}',
                    dest: 'dist'
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand:true, 
                    dot: true,
                    cwd:'./src',
                    src: ['*.html', 'partes/*.html'],
                    dest: './dist'
                }]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: 'node_modules/open-iconic/font',
                        src: ['fonts/*.*'],
                        dest: 'dist'
                    }
                ]
            }
        },

        clean: {
            build:{
                src: ['dist/']
            }
        },

        cssmin: {
            dist:{}
        },

        uglify: {
            dist:{}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },

            release: {
                files: [{
                    src: [
                        'dist/scripts/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo:{
                dest: 'dist',
                src: ['src/index.html', 'src/terminos.html', 'src/precios.html', 'src/contacto.html', 'src/about.html' ]
            },

            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function( context, block ) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/terminos.html', 'dist/precios.html', 'dist/contacto.html', 'dist/about.html' ],
            options: {
                assetsDir : ['dist', 'dist/css', 'dist/scripts' ]
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('css', ['sass'] );
    grunt.registerTask('default', ['browserSync', 'watch'] );
    grunt.registerTask('img:compress', ['imagemin'] );



    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ] );



}